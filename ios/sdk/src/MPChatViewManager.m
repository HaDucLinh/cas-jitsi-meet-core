//
//  MPChatViewManager.m
//  JitsiMeet
//
//  Created by Neolab on 3/25/19.
//  Copyright © 2019 Jitsi. All rights reserved.
//

#import <React/RCTUIManager.h>
#import <React/RCTViewManager.h>

@interface MPChatViewManager : RCTViewManager
@end

@implementation MPChatViewManager

@end
