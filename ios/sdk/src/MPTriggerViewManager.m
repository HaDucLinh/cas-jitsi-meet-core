//
//  MPTriggerViewManager.m
//  JitsiMeet
//
//  Created by Neolab on 3/25/19.
//  Copyright © 2019 Jitsi. All rights reserved.
//

#import <React/RCTUIManager.h>
#import <React/RCTViewManager.h>
#import <MPTriggerView.h>
@interface MPTriggerViewManager : RCTViewManager <EventTriggerDelegate>

@end

@implementation MPTriggerViewManager

RCT_EXPORT_VIEW_PROPERTY(onReceivedInformation, RCTDirectEventBlock)

RCT_EXPORT_MODULE()
- (UIView *)view {
    
    MPTriggerView *dummyView = [[MPTriggerView alloc] init];
    dummyView.delegate = self;
    [dummyView setupListener];
    return (UIView *) dummyView;
}
- (void)onReceivedValue:(UIView *)triggerView withData:(NSMutableDictionary *)data {
    MPTriggerView *mPTriggerView = (MPTriggerView *)triggerView;
    if (!mPTriggerView.onReceivedInformation) {
        return;
    }
    NSLog(@"will send the information!!!");
    mPTriggerView.onReceivedInformation(data);
}
@end
