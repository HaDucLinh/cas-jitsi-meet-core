//
//  MPTriggerView.m
//  JitsiMeet
//
//  Created by Neolab on 3/25/19.
//  Copyright © 2019 Jitsi. All rights reserved.
//

#import "MPTriggerView.h"
#import <React/RCTView.h>
@implementation MPTriggerView
- (void)setupListener {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"RECEIVE_INFO_MESSAGE" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMicOn:) name:@"MUTE_ON" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMicOff:) name:@"MUTE_OFF" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getLocalState:) name:@"GET_LOCAL_STATE" object:nil];
}
- (void)receiveNotification:(NSNotification *)notification {
    NSMutableDictionary * test = [[NSMutableDictionary alloc] init];
    [test setValue:@"sss" forKey:@"???"];
    [self.delegate onReceivedValue:self withData:test];
}
//THIS IS QUICK FIX FOR IMPLEMENT,SHOULD BE REFACTOR!!!
- (void)onMicOn:(NSNotification *)notification {
    NSMutableDictionary * test = [[NSMutableDictionary alloc] init];
    [test setValue:@"ON" forKey:@"MUTE"];
    [self.delegate onReceivedValue:self withData:test];
    NSLog(@"na.ngopo not mute audio");
}
- (void)onMicOff:(NSNotification *)notification {
    NSMutableDictionary * test = [[NSMutableDictionary alloc] init];
    [test setValue:@"OFF" forKey:@"MUTE"];
    [self.delegate onReceivedValue:self withData:test];
    NSLog(@"na.ngopo will mute audio");
}

- (void)getLocalState:(NSNotification *)notification {
    NSMutableDictionary * test = [[NSMutableDictionary alloc] init];
    [test setValue:@"GET_LOCAL_STATE" forKey:@"LOCALSTATE"];
    [self.delegate onReceivedValue:self withData:test];
    NSLog(@"nvt will get state local");
}
@end
