//
//  MPTriggerView.h
//  JitsiMeet
//
//  Created by Neolab on 3/25/19.
//  Copyright © 2019 Jitsi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <React/RCTView.h>
NS_ASSUME_NONNULL_BEGIN
@protocol EventTriggerDelegate <NSObject>
@optional
- (void)onReceivedValue:(UIView *)triggerView withData:(NSMutableDictionary *)data;
@end

@interface MPTriggerView : UIView
@property (nonatomic, weak) id <EventTriggerDelegate> delegate;
@property (nonatomic, copy) RCTDirectEventBlock onReceivedInformation;
- (void)setupListener;
@end

NS_ASSUME_NONNULL_END
